# HTML5 webpack template

```
"dev": "webpack --mode development",
"build": "webpack --mode production",
"build-and-beautify": "webpack --mode production && html dist/*.html --indent-size 2",
"watch": "webpack --mode development --watch",
"start": "webpack-dev-server --mode development --open",
"beautify": "html dist/*.html --indent-size 2"
```